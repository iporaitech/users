class AddFirstNameAndLastNameToIporaitechUsers < ActiveRecord::Migration
  def change
    add_column :iporaitech_users_users, :first_name, :string
    add_column :iporaitech_users_users, :last_name,  :string
  end
end
