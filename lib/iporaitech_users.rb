require 'rubygems'
require 'ransack'
require 'devise'
require 'cancancan'
require 'font-awesome-rails'
require 'bootstrap-sass'
require 'bootstrap_form'
require 'breadcrumbs_on_rails'
require 'bootstrap-sass-extras'
require 'jquery-rails'
require 'turbolinks'
require 'will_paginate'
require 'devise-bootstrap-views'

require "iporaitech_users/engine"
module IporaitechUsers
  class Engine < ::Rails::Engine
  end
end
