IporaitechUsers::Engine.routes.draw do
  devise_for :users, class_name: IporaitechUsers::User.to_s, controllers: {
    registrations: 'iporaitech_users/registrations',
    confirmations: 'iporaitech_users/confirmations'
  }, module: :devise do
    patch '/confirm' => 'confirmations#confirm'
  end

  begin
    as :user do
      patch '/user/confirmation' => 'confirmations#update', via: :patch, as: :update_user_confirmation
      get 'my/account' => 'registrations#edit', as: :edit_user_registration
      put 'my/account' => 'registrations#update', as: :user_registration
    end
  rescue
  end

  namespace 'admin' do
    resources :users
  end
end
