Rails.application.config.to_prepare do
  ::ApplicationHelper.module_eval do
    include IporaitechUsers::ApplicationHelper
  end
  unless ::IporaitechUsers::User.has_registerable_role?
    ::IporaitechUsers::User.class_eval do
      ::IporaitechUsers::User.devise_modules=(::IporaitechUsers::User.devise_modules-[:registerable])
    end
  end
end