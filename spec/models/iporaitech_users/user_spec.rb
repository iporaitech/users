require 'rails_helper'

RSpec.describe Iporaitech::User do
  
  describe 'Columns (besides Devise columns)' do
    it { should have_db_column(:first_name).of_type(:string) }
    it { should have_db_column(:last_name).of_type(:string) }
    it { should have_db_column(:roles_mask).of_type(:integer) }
    it { should have_db_column(:email).of_type(:string) }
  end
  
  describe 'Validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    # emails validations are alredy tested by devise
  end

  # roles related method such as: 
  # roles=, roles<<, User.valid_roles, roles, has_role?, has_any_role?, has_all_roles?, roles_mask, User.mask_for, is_roleFoo?, roleFoo?
  # are already tested by the role_model gem
end
