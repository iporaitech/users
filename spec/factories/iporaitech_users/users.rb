FactoryGirl.define do
  factory :user, class: ::IporaitechUsers::User do
    sequence(:first_name){|n| "First#{n}"}
    sequence(:last_name){|n| "Last#{n}"}
    email{"#{first_name}.#{last_name}@iporaitech.com".downcase}
    sequence(:password){|n| "password#{n}" }
    sequence(:password_confirmation){|n| password }
  end

  trait :client do
    roles [:client]
  end

  trait :superadmin do
    roles [:superadmin]
  end

  trait :admin do
    roles [:admin]
  end

  trait :confirmed do
    confirmed_at { Time.now }
  end
end
