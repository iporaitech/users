require "rails_helper"

module IporaitechUsers
  RSpec.describe Admin::UsersController do
    routes { IporaitechUsers::Engine.routes }
    describe "routing" do

      it "routes to #index" do
        expect(get: admin_users_path).to route_to('iporaitech_users/admin/users#index')
      end

      it "routes to #new" do
        expect(get: new_admin_user_path).to route_to("iporaitech_users/admin/users#new")
      end

      it "routes to #show" do
        expect(get: admin_user_path("1")).to route_to("iporaitech_users/admin/users#show", id: "1")
      end

      it "routes to #edit" do
        expect(get: edit_admin_user_path("1")).to route_to("iporaitech_users/admin/users#edit", id: "1")
      end

      it "routes to #create" do
        expect(post: admin_users_path).to route_to("iporaitech_users/admin/users#create")
      end

      it "routes to #update" do
        expect(put: admin_user_path("1")).to route_to("iporaitech_users/admin/users#update", id: "1")
      end

      it "routes to #destroy" do
        expect(delete: admin_user_path("1")).to route_to("iporaitech_users/admin/users#destroy", id: "1")
      end

    end
  end
end
