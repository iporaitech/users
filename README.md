# Iporaitech Users
### A generic user management gem
by [Iporaitech](http://www.iporaitech.com/).

## Getting started

This gem provides an easy way to manage users and its roles through a clean and mobile friendly (bootstrap) UI.

#### Available roles:
  * superadmin
  * admin
  * client

If you are not overiding the ability model (from cancan) the default behaviour is the following:
  * A superadmin can do everything but delete itself.
  * An admin can manage everything but users.
  * A client can only see everything!

###### Registerable roles:

The gem comes with registerable roles. By default it is a list that only includes the client role.
This means that when an user signs up, it will have only the 'client' role, thus client's permissions.

You can add more registerable roles:

```ruby
# file: config/initializers/iporaitech_users_add_registerable_roles.rb
Rails.application.config.to_prepare do
  IporaitechUsers::User.class_eval do
    roles :superadmin, :admin, :client, :new_super_cool_registerable_role
    def self.registerable_roles
      [:client, :new_super_cool_registerable_role]
    end
  end
end
```

Then when an user signs up, it will have the 'client' and 'new_super_cool_registerable_role' roles.

In case you don't want users to sign up:

```ruby
# file: config/initializers/iporaitech_users_remove_registerable_roles.rb
Rails.application.config.to_prepare do
  ::User = IporaitechUsers::User
  ::User.class_eval do
    # By default there is also an :client role, which is an registerable role
    # Here is how to remove it:
    roles :superadmin, :admin
    # IporaitechUsers will notice that there is no registerable role
    # and re configure devise to remove the registerable module.
    def self.registerable_roles
      []
    end
    # The line below is only needed if the registerable module is still loaded
    ::User.devise_modules = (::User.devise_modules-[:registerable])
  end
end
```


### Installation:

Add to Gemfile:

```ruby
# The git source from iporaitech if you still don't have
git_source(:iporaitech){ |repo_name| "http://gitlab.iporaitech.com/iporaitech/#{repo_name}.git" }
# The gem itself
gem 'iporaitech_users', '0.0.5', iporaitech: 'users'
```

Install with bundler

```
bundle install
```

Restar your app

Get the migrations. [You can skip this step if you already have a devise user model](#existing-model)

```
  rake iporaitech_users:install:migrations
  rake db:migrate
```

Add routes in config/routes.rb file:
```ruby
  mount IporaitechUsers::Engine => "/"
```

Create an superadmin user
With rails runner
```
rails r 'IporaitechUsers::User.create!(first_name: "superadmin", last_name: "superadmin", email: "super@admin.com", password: "superadmin", confirmed_at: Time.now, roles: [:superadmin])'
```

Or just past it in a rails console:
```ruby
IporaitechUsers::User.create!(first_name: "superadmin", last_name: "superadmin", email: "super@admin.com", password: "superadmin", confirmed_at: Time.now, roles: [:superadmin])
```

You can also add the superadmin role to any existing user.

And that's it! Now you can go to `/admin/users` and manage your users

### Inner Behavior
Internally iporaitech_users uses [CanCanCan](https://github.com/CanCanCommunity/cancancan) and [Devise](https://github.com/plataformatec/devise)


#### Gem default devise modules:
```ruby
 devise :database_authenticatable, :registerable,
      :recoverable, :rememberable, :trackable, :validatable, :confirmable
```

### Existing model
If you already have an user with devise:
You won't probably want to recreate tables and models or have 2 models for users.
You can decorate the Iporaitech::User class to use the same table.
Create an initializer, name it wathever you want, for example `config/initializers/iporaitech_users_decoration.rb`, and there you can go tunning the gem options:

Alias for `IporaitechUsers::User` class, of course if you already have an user model you would have to delete it:
```ruby
# file: config/initializers/iporaitech_users_decoration.rb
Rails.application.config.to_prepare do
  # Add an alias so we can keep using the User model
  ::User = ::IporaitechUsers::User
end
```

If you want to keep using your own users table skip the migrations step and decorate the model to tell it which table must be used:
```ruby
# file: config/initializers/iporaitech_users_decoration.rb
Rails.application.config.to_prepare do
  ::IporaitechUsers::User.class_eval do
    # Change the table name, so we can keep using our 'users' table
    self.table_name = "users"
  end
end
```

Or if you are using an alias:
```ruby
# file: config/initializers/iporaitech_users_decoration.rb
Rails.application.config.to_prepare do
  # Add an alias so we can keep using the User model
  ::User = ::IporaitechUsers::User

  ::User.class_eval do
    # Change the table name, so we can keep using our 'users' table
    self.table_name = "users"
  end
end
```


### User index
For user management the it is used the following scope by default:
```ruby
    scope :all_for_crud_scope, -> { where.not(with_only_registerable_roles.where_values) }    
```

So the superadmin will only see admin users and not clients that signed up in the site.
You can change this behaviour with an initializer, for example:
```ruby
# file: config/initializers/iporaitech_users_decoration.rb
Rails.application.config.to_prepare do
  IporaitechUsers::User.class_eval do
    # By default iporaitech_users does not load users with registrable roles ('client' by default)
    # Change this to manage all users
    scope :all_for_crud_scope, -> { all }  
  end
end
```

### Other configurations and options

If you want some more customization read the following example:
```ruby
# file: config/initializers/iporaitech_users_decoration.rb
Rails.application.config.to_prepare do
  # Add an alias so we can keep using the User model
  ::User = ::IporaitechUsers::User

  ::User.class_eval do
    # Change the table name, so we can keep using our 'users' table
    self.table_name = "users"

    # By default iporaitech_users does not load users with registrable roles ('client' by default)
    # Change this to see and manage all users
    scope :all_for_crud_scope, -> { all }

    # By default there is also an :client role, which is an registerable role
    # Here is how to remove it:
    roles :superadmin, :admin
    # IporaitechUsers will notice that there is no registerable role
    # and re configure devise to remove the registerable module.
    def self.registerable_roles
      []
    end

  end

  # Change controllers layouts
  ::IporaitechUsers::Admin::UsersController.layout "application_admin"
  ::IporaitechUsers::ApplicationController.layout "application"

  # After sign in behaviour
  ::IporaitechUsers::ApplicationController.class_eval do
    def after_sign_in_path_for(resource)
      if resource.has_role?(:admin) || resource.has_role?(:superadmin)
        admin_path
      else
        super
      end
    end    
  end


  # Override devise behaviour defined by gem
  # User authentication modules
  ::IporaitechUsers::User.devise_modules=[:database_authenticatable, :recoverable, :rememberable, :trackable,  :validatable,  :confirmable,  :timeoutable]

end
```
## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
