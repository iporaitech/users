Given(/^that I am logged in as an user with "(.*?)" role$/) do |roles|
  @current_user = FactoryGirl.create(:user, :confirmed, roles: parse_roles(roles))
  login_as(@current_user)
end

Given(/^I go to the user list$/) do
  visit(iporaitech_users.admin_users_path)
end

Given(/^I press the "(.*?)" link$/) do |link|
  begin 
    click_link(link.downcase, match: :first)
  rescue
    click_link(link.camelcase, match: :first)
  end
end

When(/^I press the Edit link$/) do 
  find('.edit-link', match: :first).trigger('click')
end

Given(/^I press the next page link$/) do
  click_link(CGI.unescapeHTML(I18n.t!("will_paginate.next_label")), match: :first)
end

Given(/^I press the "(.*?)" button$/) do |button|
  click_button(button)
end

Then(/^I should go to the Create form$/) do
  wait_page
  expect(current_path).to eq(iporaitech_users.new_admin_user_path)
end

When(/^I fill the email field "(.*?)"$/) do |email|
  within("#new_user") do
    fill_in(:first_name, with: FactoryGirl.build(:user).first_name)
    fill_in(:last_name, with: FactoryGirl.build(:user).last_name)
    fill_in(:email, with: email)
  end
end

When(/^I check the roles (.*)$/) do |roles|
  parse_roles(roles).each do |role|
    check("user_roles_#{role}")
  end
end

Then(/^a new user with email "(.*?)" and (.+) roles should be created in the db$/) do |email, roles|
  wait_page
  expect(::IporaitechUsers::User.find_by(email: email).roles.to_a).to match_array(parse_roles(roles))
end

Then(/^a new user with email "(.*?)" and  roles should be created in the db$/) do |email|
  wait_page
  expect(::IporaitechUsers::User.find_by(email: email).roles.to_a).to match_array([])
end

Given(/^an user with email "(.*?)" and (.+) roles$/) do |email, roles|
  @user = FactoryGirl.create(:user, :confirmed, roles: parse_roles(roles), email: email)
end

Then(/^I should go to the Edit form$/) do
  wait_page
  expect(current_path).to eq(iporaitech_users.edit_admin_user_path(@user))
end

When(/^I uncheck the roles (.*)$/) do |roles|
  parse_roles(roles).each do |role|
    uncheck("user_roles_#{role}")
  end
end

When(/^I change the email to (.+)$/) do |email|
  @email = email
  within("#edit_user"){ fill_in(:email, with: email) } 
end

Then(/^the user should be updated$/) do
  wait_page
  expect(@user.updated_at).to be < @user.reload.updated_at
end

Then(/^it should have only the (.+) roles$/) do |roles|
  expect(@user.roles.to_a).to match_array(parse_roles(roles))
end

Then(/^it should have only the  roles$/) do
  expect(@user.roles.to_a).to match_array([])
end

Then(/^the email should have been updated$/) do
  expect(@user.unconfirmed_email).to eq(@email) if @user.email != @email
end

Given(/^the following users:$/) do |table|
  table.hashes.each do |row|
    FactoryGirl.create(:user, :confirmed, email: row[:email], roles: [:admin])
  end
end

When(/^I fill the search field with "(.*?)"$/) do |search_pattern|
  find('input.ransack-search').set(search_pattern)
end

Then(/^I should see users with:$/) do |table|
  wait_page
  table.hashes.each_with_index do | row, index |
    within "#listing_users tbody tr:nth-child(#{index+1})" do
      expect(page).to have_selector("tr td.email", text: row[:email])
    end
  end
end