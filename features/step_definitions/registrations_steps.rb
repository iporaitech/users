Given(/^that I am not logged in$/) do
  logout(:user)
  logout(:iporaitech_user)
end

When(/^I go to the registration page$/) do
  visit iporaitech_users.new_user_registration_path
end

When(/^fill the registration fields$/) do
  @random_user = FactoryGirl.build(:user)
  within("#new_registration_form") do
    fill_in(:first_name, with: @random_user.first_name)
    fill_in(:last_name, with: @random_user.last_name)
    fill_in(:email, with: @random_user.email)
  end
end

Then(/^a user should be created$/) do
  @recently_created_user = ::IporaitechUsers::User.find_by(email: @random_user.email)
  expect(@recently_created_user).to_not be_nil 
end

Then(/^it should only have the registrable roles$/) do
  user = @recently_created_user || @current_user
  expect(user.reload.roles.to_a).to match_array(::IporaitechUsers::User.registerable_roles)
end

When(/^I go to the edit account page$/) do
  visit iporaitech_users.edit_user_registration_path
end

When(/^change some account field$/) do
  @random_first_name = FactoryGirl.build(:user).first_name
  within("#edit_user") do
    fill_in(:first_name, with: @random_first_name)
  end
end

And(/^properly fill the current password field$/) do
  within("#edit_user") do
    fill_in(:user_current_password, with: @current_user.password)
  end
end

Then(/^my user should be updated$/) do
  expect(@current_user.reload.first_name).to eql @random_first_name
end

Given(/^an unconfirmed user$/) do
  @unconfirmed_user = FactoryGirl.create(:user, :client, {
    password: nil,
    password_confirmation: nil,
    encrypted_password: "",
    confirmed_at: nil
  }) 
end

When(/^I open the confirmation email$/) do 
  open_email(@unconfirmed_user.email)
end

When(/^I open the confirmation mail for the new email address$/) do 
  open_email(@current_user.unconfirmed_email)
end

When(/^press the confirmation link$/) do
  click_first_link_in_email
end

When(/^I set my own password$/) do
  @random_password = FactoryGirl.build(:user).password
  within("#edit_user") do
    fill_in(:user_password, with: @random_password)
    fill_in(:user_password_confirmation, with: @random_password)
  end
  click_button("Activate account")
end

Then(/^my account should be confirmed$/) do
  expect(@unconfirmed_user.reload.confirmed?).to be(true)
end

Then(/^I should go to the home page$/) do
  expect(current_path).to eq(main_app.root_path)
end

When(/^change my email$/) do
  @new_email = FactoryGirl.build(:user).email
  within("#edit_user") do
    fill_in(:user_email, with: @new_email)
  end  
end

Then(/^my user should be updated with its new email$/) do
  expect(@current_user.reload.unconfirmed_email).to eql @new_email
end

Then(/^my account should be reconfirmed$/) do
  expect(@current_user.reload.unconfirmed_email?).to be(false)
end