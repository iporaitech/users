# language: en
@javascript

Feature: Registrations
  As a not registered user I would like to sign up as a client

Scenario: Create account
  Given that I am not logged in
  When I go to the registration page
  And fill the registration fields
  When I press the "Sign up" button
  Then a user should be created
  And it should only have the registrable roles

Scenario: Confirmation
  Given that I am not logged in
  And an unconfirmed user
  When I open the confirmation email
  And press the confirmation link
  And I set my own password
  Then my account should be confirmed
  And I should go to the home page

Scenario: Edit account
  Given that I am logged in as an user with "client" role
  When I go to the edit account page
  And change some account field
  And properly fill the current password field
  When I press the "Update" button
  Then my user should be updated
  And it should only have the registrable roles

Scenario: Edit email
  Given that I am logged in as an user with "client" role
  When I go to the edit account page
  And change my email
  And properly fill the current password field
  When I press the "Update" button
  Then my user should be updated with its new email
  And it should only have the registrable roles  
  When I open the confirmation mail for the new email address
  And press the confirmation link
  Then my account should be reconfirmed
  And I should go to the home page