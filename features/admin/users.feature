# language: en
@javascript

Feature: User CRUD
  As a superadmin I would like to manage users and their roles 

Background:
  Given that I am logged in as an user with "superadmin" role

Scenario Outline: Creating user
  When I go to the user list
  And I press the "New user" link
  Then I should go to the Create form
  When I fill the email field "test@iporaitech.com"
  And I check the roles <roles>
  And I press the "Create" button
  Then a new user with email "test@iporaitech.com" and <roles> roles should be created in the db

 Examples:
    |     roles                   |
    |  admin, client              |
    |  admin, superadmin          |
    |  admin, client, superadmin  |
    |  superadmin, client         |
    |  superadmin, admin          |
    |  superadmin                 |
    |  client                     |
    |  admin                      |
    |                             |

Scenario Outline: Editing user
  Given an user with email "test1@test.com" and <initial> roles
  When I go to the user list
  And I press the Edit link
  Then I should go to the Edit form
  When I uncheck the roles <unchecked>
  And I check the roles <checked>
  And I change the email to <email>
  And I press the "Update" button
  Then the user should be updated
  And it should have only the <remaining> roles
  And the email should have been updated

 Examples:
    |      initial                   |    unchecked                |      checked            |    remaining                |        email       |
    |   admin, client                |      client                 |                         |      admin                  |    test1@test.com  |
    |   superadmin, admin, client    |      client                 |                         |      admin, superadmin      |    test2@test.com  |
    |   admin, client, superadmin    |      client, superadmin     |                         |      admin                  |    test3@test.com  |
    |   client, superadmin           |      client                 |    superadmin, admin    |      admin, superadmin      |    test1@test.com  |
    |   admin                        |                             |      superadmin         |      admin, superadmin      |    22222@test.com  |
    |   client, superadmin           |      client                 |      admin              |      admin, superadmin      |    test1@test.com  |
    |   admin                        |      admin                  |                         |                             |    testo@test.com  |

Scenario: Search with pagination
  Given the following users:
    |       email       |
    |   test1@test.com  |
    |   tets2@test.com  |
    |   test3@test.com  |
    |   test4@test.com  |
    |   tets5@test.com  |
    |   test6@test.com  |
    |   test7@test.com  |
    |   tets8@test.com  |
    |   test9@test.com  |
    |   tes10@test.com  |
    |   tet11@test.com  |
    |   tes12@test.com  |
    |   ex001@mple.com  |
    |   ex002@mple.com  |
    |   ex003@mple.com  |
  When I go to the user list
  And I fill the search field with "test"
  And I press the "Search" button
  Then I should see users with:
    |       email       |
    |   test1@test.com  |
    |   tets2@test.com  |
    |   test3@test.com  |
    |   test4@test.com  |
    |   tets5@test.com  |
    |   test6@test.com  |
    |   test7@test.com  |
    |   tets8@test.com  |
    |   test9@test.com  |
    |   tes10@test.com  |
  When I press the next page link
  Then I should see users with:
    |       email       |
    |   tet11@test.com  |
    |   tes12@test.com  |
