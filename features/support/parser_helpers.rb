module ParserHelpers
  # TODO: use Step Argunment Transforms (https://github.com/cucumber/cucumber/wiki/Step-Argument-Transforms) instead of this
  def parse_roles(roles)
    roles.split(", ").map(&:to_sym)
  end
end
World(ParserHelpers)