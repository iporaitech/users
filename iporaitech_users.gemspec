$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "iporaitech_users/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "iporaitech_users"
  s.version     = IporaitechUsers::VERSION
  s.authors     = ["Edipo Vinicius da Silva"]
  s.email       = ["edipo@iporaitech.com"]

  s.summary       = %q{Basic User CRUD}
  s.description   = %q{User CRUD with Authentication & Authorization using devise and cancancan.}
  s.homepage      = "http://iporaitech.com"
  s.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if s.respond_to?(:metadata)
    s.metadata['allowed_push_host'] = "TODO: Set to 'http://iporaitech.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end


  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.test_files = Dir["spec/**/*"]
  s.add_dependency "rails", "~> 4.2"
  # pagination, search and related stuff
  s.add_dependency 'pg', '~> 0.18'
  s.add_dependency 'will_paginate', '~> 3.0'
  s.add_dependency 'ransack', '~> 1.6'

  # Bootstrap forms with nested_form support
  s.add_dependency 'bootstrap_form', '~> 2.3'
  s.add_dependency 'bootstrap-sass-extras', '~> 0.0.6'

  # Authentication & Authorization
  s.add_dependency 'devise', '~> 3.4'
  s.add_dependency 'devise-bootstrap-views', '~> 0.0.6'
  s.add_dependency 'cancancan', '~> 1.10'
  s.add_dependency 'role_model', '~> 0.8'

  # Javascript and CSS assets
  s.add_dependency 'jquery-rails', '~> 4.0'
  s.add_dependency 'turbolinks', '~> 2.5'
  s.add_dependency 'jbuilder', '~> 2.0'
  s.add_dependency 'font-awesome-rails', '~> 4.3'
  s.add_dependency 'bootstrap-sass', '~> 3.3'
  s.add_dependency "breadcrumbs_on_rails", '~> 2.3'

  # dev deps
  s.add_development_dependency "bundler", "~> 1.7"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "rspec", '~> 3.1'
  s.add_development_dependency 'factory_girl_rails', '~> 4.5'
  s.add_development_dependency 'rspec-rails', '~> 3.1'
  s.add_development_dependency 'capybara', '~> 2.4'
  # s.add_development_dependency "sqlite3"
end
