json.array!(@users) do |admin|
  json.extract! admin, :id, :email, :first_name, :last_name, :superadmin
  json.url admin_url(admin, format: :json)
end
