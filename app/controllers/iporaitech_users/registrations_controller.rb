module IporaitechUsers
  class RegistrationsController < Devise::RegistrationsController
    
    before_filter :configure_permitted_parameters_for_sign_up, only: :create
    
    before_filter :configure_permitted_parameters_for_account_update, only: :update

    protected

      # Sets the resource creating an instance variable
      def resource=(new_resource)
        new_resource.roles = ::IporaitechUsers::User.registerable_roles if new_resource.new_record?
        instance_variable_set(:"@#{resource_name}", new_resource)
      end     

      def configure_permitted_parameters_for_sign_up
        devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:phone, :first_name, :last_name, :email, :password) }
      end

      def configure_permitted_parameters_for_account_update  
        devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:phone, :first_name, :last_name, :email, :password, :password_confirmation, :current_password) } 
      end
      
  end 
end