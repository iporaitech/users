module IporaitechUsers
  class Admin::UsersController < ::ApplicationController
    helper ::IporaitechUsers::ApplicationHelper
    load_and_authorize_resource class: ::IporaitechUsers::User

    before_action :set_user, only: [:show, :edit, :update, :destroy]
    add_breadcrumb :index, :admin_users_path

    def index
      @query = ::IporaitechUsers::User.all_for_crud_scope.search(query_param)
      @users = @query.result.page(page_param)    
    end

    def show
      add_breadcrumb @user.email, admin_user_path(@user)
    end

    def new
      add_breadcrumb :new, new_admin_user_path
      @user = ::IporaitechUsers::User.new
    end

    def edit
      add_breadcrumb :edit, edit_admin_user_path
    end

    def create
      @user = ::IporaitechUsers::User.new(user_params)
      respond_to do |format|
        if @user.save 
          format.html { redirect_to admin_user_path(@user), notice: I18n.t!('views.users.flash_messages.successfully.created') }
          format.json { head :no_content }
        else
          format.html { render action: 'new' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end                           
    end

    def update
      respond_to do |format|
        if _update_user(@user)
          flash[:alert] = I18n.t!("views.users.flash_messages.email_confirmation_change", email: @user.unconfirmed_email) if @user.unconfirmed_email.present?
          format.html { redirect_to admin_user_path(@user), notice: I18n.t!('views.users.flash_messages.successfully.updated') }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end                           
    end

    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to admin_users_path, notice: I18n.t!('views.users.flash_messages.successfully.destroyed') }
        format.json { head :no_content }
      end
    end

    private

      def set_user
        @user =  ::IporaitechUsers::User.find_by(id: params[:id]) || current_user
      end

      def user_params
        permitted = [:email, :first_name, :last_name]
        permitted << {roles: ::IporaitechUsers::User.valid_roles} if can?(:change_roles, @user)
        params.require(:user).permit(*permitted)
      end

      def _update_user user
        user.roles = [] if can?(:change_roles, user)
        # https://github.com/plataformatec/devise/wiki/How-To%3a-Allow-users-to-edit-their-account-without-providing-a-password
        update_method = can?(:manage, user) ? :update_without_password : :update
        # only superuser can update without password
        user.send(update_method, user_params)
      end

      def page_param
        params.require(:page) if params[:page]
      end
      
      def query_param
        params.require(:q) if params[:q]
      end    
  end
end
