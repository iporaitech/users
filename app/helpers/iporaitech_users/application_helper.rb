
module IporaitechUsers
  module ApplicationHelper

    def devise_custom_error_messages!(resource=current_user)
      return '' if !resource.is_a?(User) || resource.errors.empty?

      messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
      sentence = I18n.t('errors.messages.not_saved',
        count: resource.errors.count,
        resource: resource.class.model_name.human.downcase)

      html = <<-HTML 
      <div class="alert alert-danger alert-block"> 
        <strong>#{sentence}</strong>
        #{messages}
      </div>
      HTML

      html.html_safe
    end    
    
    def page_header content
      content_tag(:div, class: :page_header) do
        content_tag(:h1, t('.title', default: content))
      end
    end

    def page_sub_header content
      content
    end

    def panel_heading_search_form_for(query,custom_path, *fields)
      text_field_name = fields.join('_or_').concat('_cont')
      scope = query.context.klass.table_name
      searchable = fields.map do |field|
        I18n.t!(["activerecord.attributes.#{scope.singularize}", field].join('.'))
      end
      search_form_for(query, url: custom_path) do |sf|
        capture do
          concat raw('<div class="form-group input-group search-group" >')
            concat sf.text_field(text_field_name, class: 'form-control ransack-search',
              placeholder: I18n.t!('views.search_by',
                entity: searchable.join(" #{I18n.t!('words.or')} ")),
              autocomplete: :off)
            concat raw('<span class="input-group-btn">')
              concat sf.button(raw(" #{I18n.t!('views.search', entity: I18n.t!('activerecord.models.user.one')).capitalize}"), class: 'btn btn-default')
            concat raw('</span>')
          concat raw('</div>')
        end
      end
    end

    def show_date(d)
      l(d, format: :long) unless d.nil?
    end
    
    def show_roles user
      user.roles.to_a.map{|role| t("activerecord.attributes.roles.#{role}").capitalize }.join(', ')
    end

    
    ALERT_TYPES = [:success, :info, :warning, :danger] unless const_defined?(:ALERT_TYPES)

    def bootstrap_flash(options = {})
      flash_messages = []
      flash.each do |type, message|
      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
        next if message.blank?

        type = type.to_sym
        type = :success if type == :notice
        type = :danger  if type == :alert
        type = :danger  if type == :error
        next unless ALERT_TYPES.include?(type)

        Array(message).each do |msg|
          text = content_tag(:div,
                             content_tag(:button, raw("&times;"), :class => "close", "data-dismiss" => "alert") +
                             msg, id: 'flash', :class => "alert fade in alert-#{type} #{options[:class]}")
          flash_messages << text if msg
        end
      end
      flash_messages.join("\n").html_safe
    end
  end
end
