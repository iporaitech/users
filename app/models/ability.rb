class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :edit, :update, :show, to: :modify
    @user = user || ::Iporaitech::User.new
    @user.roles.to_a.reverse.each { |role| send(role) }
    can :modify, ::Iporaitech::User do |other| 
      @user.id == other.id
    end
    cannot :destroy, ::Iporaitech::User do |other| 
      @user.id == other.id
    end
  end

  def client
    can :read, :all
    cannot :manage, ::Iporaitech::User
    cannot :change_roles, ::Iporaitech::User
  end

  def admin
    can :manage, :all
    client # we want to add client's limitations
  end

  def superadmin
    admin
    can :manage, :all
  end
end