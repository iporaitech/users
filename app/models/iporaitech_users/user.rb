require 'role_model'
module IporaitechUsers
  class User < ActiveRecord::Base
    #
    # Authorization management
    #
    include RoleModel
    # declare the valid roles -- do not change the order if you add more
    # roles later, always append them at the end!
    roles :superadmin, :admin, :client

    # Include devise modules. Others available are:
    # :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
      :recoverable, :rememberable, :trackable, :validatable, :confirmable

    # Valiations
    validates_presence_of :first_name, :last_name

    def self.registerable_roles
      [:client]
    end

    def self.has_registerable_role?
      ! self.registerable_roles.size.zero?
    end

    # TODO: test this
    def self.with_any_role(*roles)
      query = roles.map do |role|
        "roles_mask & #{User.mask_for(role)} > 0"
      end.join(" OR ")  
      where(query)
    end

    def self.only_with_role(*roles)
      where("roles_mask = #{mask_for(roles)}")
    end

    scope :with_only_registerable_roles, -> {  only_with_role(*registerable_roles) }

    # This method will be used in the admin users controller
    scope :all_for_crud_scope, -> { where.not(with_only_registerable_roles.where_values) }

    # email only sign up
    # see : https://github.com/plataformatec/devise/wiki/How-To:-Email-only-sign-up
    # def password_required?
    #   super unless confirmed_at.nil?
    # end
    def password_match?
      self.errors[:password] << "can't be blank" if password.blank?
      self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
      self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
      password == password_confirmation && !password.blank?
    end
    # / email only sign up

    # confirmation flow
    # https://github.com/plataformatec/devise/wiki/How-To:-Override-confirmations-so-users-can-pick-their-own-passwords-as-part-of-confirmation-activation
    def password_required?
      # Password is required if it is being set, but not for new records
      if !persisted? 
        false
      else
        !password.nil? || !password_confirmation.nil?
      end
    end

    def only_if_unconfirmed
      pending_any_confirmation {yield}
    end
    # new function to set the password without knowing the current password used in our confirmation controller. 
    def attempt_set_password(params)
      p = {}
      p[:password] = params[:password]
      p[:password_confirmation] = params[:password_confirmation]
      update_attributes(p)
    end
    # new function to return whether a password has been set
    def has_no_password?
      self.encrypted_password.blank?
    end
    # / confirmation flow

    self.per_page = 10
  end
end

unless self.class.const_defined?(:Iporaitech)
  module ::Iporaitech
  end
end
::Iporaitech::User = IporaitechUsers::User